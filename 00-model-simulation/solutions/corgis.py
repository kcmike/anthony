num_ponds = int(input())
for p in range(num_ponds):
	(num_dogs, num_friendships) = map(int, input().split(" "))
	friends = {}
	for d in range(num_dogs):
		friends[d + 1] = [d + 1]
	for f in range(num_friendships):
		(dog1, dog2) = map(int, input().split(" "))
		friends[dog1].append(dog2)
		friends[dog2].append(dog1)
	total_reachability = 0
	for d in friends:
		visited = [False] * (len(friends) + 1)
		to_visit = [d]
		while to_visit:	# while to_visit is not empty
			n = to_visit.pop()
			if visited[n - 1]:
				continue
			visited[n - 1] = True
			total_reachability += 1
			to_visit.extend(friends[n])
	print('Pond #%d: %.3f' % (p + 1, total_reachability / num_dogs))
