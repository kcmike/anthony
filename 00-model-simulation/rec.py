#def power(x, y):
#	"""x and y are both positive integers. Calculate x^y"""
#	z = 1
#	for i in range(y):
#		z *= x
#	return z

def power(x, y):
	if y == 0:
		return 1
	elif y % 2 == 0:
		z = power(x, y // 2)
		return z * z
	else:
		return x * power(x, y - 1)
