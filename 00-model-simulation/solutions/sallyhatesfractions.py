cases = int(input())
for c in range(cases):
    num = int(input())
    array = list(map(int, input().split(' ')))
    statement = True
    for i in array:
        for j in array:
            if i % j == 0 or j % i == 0:
                pass
            else:
                statement = False
                break
    if statement:
        print("Array #{}: This array is bae!".format(c+1))
    else:
        print("Array #{}: Go away!".format(c+1))
        
