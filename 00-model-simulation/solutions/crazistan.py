num_candidates = int(input())
votes_from = {}
votes_for = {}
for i in range(1, num_candidates + 1):
	votes_from[i] = []
for i in range(1, num_candidates + 1):
	v = int(input())
	votes_from[v].append(i)
	votes_for[i] = v
no_votes = [ k for k, v in votes_from.items() if len(v) == 0 ]
total_reachable = {}
for candidate in no_votes:
	reached = [False] * (num_candidates + 1)
	current_node = candidate
	while reached[current_node] == False:
		reached[current_node] = True
		current_node = votes_for[current_node]
	total_reachable[candidate] = len([ r for r in reached if r == True ])
m = list(total_reachable.keys())[0]
for k, v in total_reachable.items():
	if v < total_reachable[m]:
		m = k
print(m)
#print(min(total_reachable, key=total_reachable.get))
