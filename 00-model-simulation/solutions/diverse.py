import sys

def diverse(n):
	n = str(n)
	counters = [0] * 10
	for i in n:
		counters[int(i)] += 1
	k = max(counters)
	for i in range(len(n)):
		j = len(n) - i - 1
		if counters[int(n[j])] == k:
			return (k, j)

def smallest_k_diverse(n, target_k):
	if n >= 9876543211 and target_k == 1:
		return 'Find a different k'
	while True:
		k, j = diverse(n)
		if k <= target_k:
			return n
		zeroes = len(str(n)) - j - 1
		x = 10 ** zeroes
		y = n % x
		n = n + x - y

with open('diverse.in') as f:
	(c,) = map(int, f.readline().split())
	for i in range(c):
		n, k = map(int, f.readline().split())
		print(smallest_k_diverse(n, k))
