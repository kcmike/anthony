from operator import attrgetter

class Person:
	def __init__(self, x, y, d):
		self.x = x
		self.y = y
		self.direction = d
	def __repr__(self):
		return '({}, {}, {})'.format(self.x, self.y, self.direction)

y = int(input())
for ii in range(1, y + 1):
	p, q = map(int, input().split(" "))
	ns = []
	ss = []
	es = []
	ws = []
	for e in range(p):
		x, y, d = input().split(" ")
		p = Person(int(x), int(y), d)
		if d == 'N':
			ns.append(p)
		elif d == 'S':
			ss.append(p)
		elif d == 'E':
			es.append(p)
		elif d == 'W':
			ws.append(p)
	ns.sort(key=attrgetter('y'), reverse=False)
	ss.sort(key=attrgetter('y'), reverse=True)
	es.sort(key=attrgetter('x'), reverse=False)
	ws.sort(key=attrgetter('x'), reverse=True)
	yaxis_ns = []
	nsi = 0
	for i in range(q + 1):
		while nsi < len(ns) and ns[nsi].y < i:
			nsi += 1
		yaxis_ns.append(nsi)
	yaxis_ss = []
	ssi = 0
	for i in range(q, -1, -1):
		while ssi < len(ss) and ss[ssi].y > i:
			ssi += 1
		yaxis_ss.append(ssi)
	yaxis_ss.reverse()
	yaxis = list(map(lambda x: x[0] + x[1], zip(yaxis_ns, yaxis_ss)))
	xaxis_es = []
	esi = 0
	for i in range(q + 1):
		while esi < len(es) and es[esi].x < i:
			esi += 1
		xaxis_es.append(esi)
	xaxis_ws = []
	wsi = 0
	for i in range(q, -1, -1):
		while wsi < len(ws) and ws[wsi].x > i:
			wsi += 1
		xaxis_ws.append(wsi)
	xaxis_ws.reverse()
	xaxis = list(map(lambda x: x[0] + x[1], zip(xaxis_es, xaxis_ws)))
	ymax = 0
	xmax = 0
	for i in range(q + 1):
		if yaxis[i] > yaxis[ymax]:
			ymax = i
		if xaxis[i] > xaxis[xmax]:
			xmax = i
	print("Case #%d:" % ii, xmax, ymax)
